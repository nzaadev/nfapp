package com.imanancin.nfapp;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Created by : iman
 * Date : 06/03/24
 */
public class Database {

    ArrayList<Level> levels = new ArrayList<>();
    int day;

    public Database() {
        levels.add(new Level("Clown", R.drawable.clown, 1));
        levels.add(new Level("Noob", R.drawable.noob, 3));
        levels.add(new Level("Chad", R.drawable.chad, 7));
        levels.add(new Level("Giga chad", R.drawable.gigachad, 30));
    }
    public ArrayList<Level> getLevels() {
        return levels;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public Level getCurrentLevel() {

        Level level = null;

        for(int i = levels.size()-1; i>=0; i--) {
            if(day==0) {
                level = levels.get(0); break;
            }
            if(levels.get(i).getDay() <= day ) {
                level = levels.get(i); break;
            }
        }

        Log.e("TAG", "getCurrentLevel: " + level.toString() );
        return level;
    }

}
