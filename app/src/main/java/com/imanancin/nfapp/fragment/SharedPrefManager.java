package com.imanancin.nfapp.fragment;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by : iman
 * Date : 06/03/24
 */
public class SharedPrefManager {
    private static final String pref_name  = "mypref";
    private static final String key_name  = "myxpref";
    static private SharedPreferences sharedPreferences;

    public static void init(Context context) {
        sharedPreferences = context.getSharedPreferences(pref_name, Context.MODE_PRIVATE);
    }

    public static void saveLongPref(Long val) {
        sharedPreferences.edit().putLong(key_name, val).apply();
    }

    public static void removeLongPref(String key) {
        sharedPreferences.edit().remove(key_name).apply();
    }

    public static Long getLongPref(String key) {
        return sharedPreferences.getLong(key_name, 0);
    }
}
