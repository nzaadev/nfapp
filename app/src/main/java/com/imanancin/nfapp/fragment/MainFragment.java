package com.imanancin.nfapp.fragment;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.imanancin.nfapp.Database;
import com.imanancin.nfapp.Level;
import com.imanancin.nfapp.MainActivity;
import com.imanancin.nfapp.R;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class MainFragment extends Fragment {

    private MainViewModel mViewModel;
    TextView timer;
    TextView tvLevel;
    TextView day;
    ImageView ivLevel;
    FloatingActionButton floatingActionButton;
    MainActivity mainActivity;
    private boolean _isRunning  = false;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        mainActivity = (MainActivity) getActivity();
        mViewModel = new ViewModelProvider(mainActivity).get(MainViewModel.class);
        Database database = mViewModel.database;
        List<Level> levels = database.getLevels();
        // Nav view levels

        for(int i = 0; i<levels.size(); i++) {
            View v = getLayoutInflater().inflate(R.layout.item_level, null);
            ((TextView)v.findViewById(R.id.tvNameLevel)).setText(levels.get(i).getName());
            ((TextView)v.findViewById(R.id.tvDayLevel)).setText(levels.get(i).getDay() + "+ Days");
            ((ImageView)v.findViewById(R.id.ivLevel)).setImageResource(levels.get(i).getImage());
            mainActivity.layout.addView(v);
        }

        timer = view.findViewById(R.id.timer);
        ivLevel = view.findViewById(R.id.ivLevel);
        tvLevel = view.findViewById(R.id.tvLevel);
        day = view.findViewById(R.id.day);

        floatingActionButton = view.findViewById(R.id.startTimer);

        // observe timer
        mViewModel.timers.observe(getViewLifecycleOwner(), c -> {
            timer.setText(formatter(c));
            int d = (int)TimeUnit.MILLISECONDS.toDays(c);
            day.setText(String.valueOf(d));
            mViewModel.database.setDay(d);
            mViewModel.getCurrentLevel();
        });



        mViewModel.level.observe(getViewLifecycleOwner(), level -> {
            tvLevel.setText(level.getName());
            ivLevel.setImageResource(level.getImage());
            Log.e("TAG", "level" );
        });



        // start timer
        if(SharedPrefManager.getLongPref("start") != 0) {
            mViewModel.initAll();
        }

        // lacak perubahan apakah timer bejalan
        mViewModel.isRunning.observe(getViewLifecycleOwner(), isRunning -> {
            if(isRunning) {
                _isRunning = true;
                floatingActionButton.setImageResource(R.drawable.baseline_pause_circle_24);
            } else {
                _isRunning = false;
                floatingActionButton.setImageResource(R.drawable.baseline_play_arrow_24);
            }
        });

        // handle FAB
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(_isRunning) {
                    relapseConfirmation();
                } else {
                    mViewModel.startTimer();
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        mViewModel.getCurrentLevel();
    }

    private void relapseConfirmation() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mainActivity);
        alertDialog.setTitle("Apakah kamu mau bercocok?");
        alertDialog.setView(getLayoutInflater().inflate(R.layout.editext_confirm, null));
        alertDialog.setPositiveButton("OK", (dialog, which) -> {
            // reset
            mViewModel.stopTimer();
        });

        alertDialog.setNegativeButton("Nggak", (dialog, which) -> {});
        alertDialog.show();
    }
    @SuppressLint("DefaultLocale")
    private String formatter(long millis) {
        return String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis) -  TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millis)),
                TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
    }


}