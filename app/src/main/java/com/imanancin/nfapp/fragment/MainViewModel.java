package com.imanancin.nfapp.fragment;

import android.annotation.SuppressLint;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.imanancin.nfapp.Database;
import com.imanancin.nfapp.Level;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class MainViewModel extends ViewModel {

    private final MutableLiveData<Level> _level = new MutableLiveData<>();
    public LiveData<Level> level = _level;
    private final MutableLiveData<Long> countDown = new MutableLiveData<>(2L);
    public LiveData<Long> timers = countDown;

    private Timer timer;

    private final MutableLiveData<Boolean> _isRunning = new MutableLiveData<>(false);
    public LiveData<Boolean> isRunning = _isRunning;
    long start = 0;
    Database database;
    


    public MainViewModel() {
        database = new Database();
        getCurrentLevel();
    }

    public void startTimer() {
        if(_isRunning.getValue() == Boolean.TRUE) {
            stopTimer();
            return;
        }

        initAll();

//        Calendar calendar = Calendar.getInstance();
////        calendar.setTime(new Date());
//        // reset hour, minutes, seconds and millis
////        calendar.set(Calendar.HOUR_OF_DAY, 0);
////        calendar.set(Calendar.MINUTE, 0);
////        calendar.set(Calendar.SECOND, 0);
////        calendar.set(Calendar.MILLISECOND, 0);
//
//        calendar.add(Calendar.HOUR_OF_DAY, 1);



    }

    public void initAll() {
        initializePref();
        setTimer();
    }

    private void setTimer() {
        timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                long diff = getDiff();
                countDown.postValue(diff);
            }
        };

        _isRunning.setValue(true);
        timer.scheduleAtFixedRate(timerTask, 0, 1000);
    }

    private void initializePref() {
        long val = SharedPrefManager.getLongPref("start");
        if( val != 0) {
            start = val;
        } else {
            long curr = System.currentTimeMillis() - 259_190_000;
            // set timer
            SharedPrefManager.saveLongPref(curr);
            start = curr;
        }
    }


    public void stopTimer() {
        if(timer != null) {
            SharedPrefManager.removeLongPref("start");
            start = 0;
            countDown.postValue(0L);
            timer.cancel();
            timer = null;
            _isRunning.setValue(false);
        }
    }

    public void getCurrentLevel() {
        _level.setValue(database.getCurrentLevel());
    }

    public long getDiff() {
        return System.currentTimeMillis() - start;
    }


}