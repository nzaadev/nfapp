package com.imanancin.nfapp;

/**
 * Created by : iman
 * Date : 06/03/24
 */
public class Level {
    String name;
    int image;
    int day;


    public Level(String name, int image, int day) {
        this.name = name;
        this.image = image;
        this.day = day;
    }

    public String getName() {
        return name;
    }

    public int getImage() {
        return image;
    }

    public int getDay() {
        return day;
    }

    @Override
    public String toString() {
        return "Level{" +
                "name='" + name + '\'' +
                ", image=" + image +
                ", day=" + day +
                '}';
    }
}
